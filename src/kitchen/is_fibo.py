__author__ = 'kppatel'

saved_fibs = {0: 0, 1: 1}


def fib(n):
	if n not in saved_fibs:
		saved_fibs[n] = fib(n - 1) + fib(n - 2)
	return saved_fibs[n]


def check_fibo(n):
	fib(n)
	fib_list = [saved_fibs.get(i) for i in sorted(saved_fibs.keys())]
	print(fib_list)
	if n in fib_list:
		return "IsFibo"
	else:
		return "IsNotFibo"


from math import sqrt


def is_perfect_square(n):
	s = sqrt(n)
	return s * s == n


# 5 * n^2 +- 4 should be perfect square
def check_fibo_number(n):
	if is_perfect_square(5 * n * n + 4) or is_perfect_square(5 * n * n - 4):
		return "IsFibo"
	else:
		return "IsNotFibo"


if __name__ == "__main__":
	from sys import argv

	n = int(argv[1])
	print(check_fibo_number(n))

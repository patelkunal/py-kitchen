def compute_unfairness(n, k, numbers):
	if k == 1:
		return 0
	elif k == n:
		return max(numbers) - min(numbers)
	else:
		sorted_numbers = sorted(numbers)
		min_unfairness = max(numbers)
		for i in range(n - k + 1):
			
			if min_unfairness > (sorted_numbers[i + k - 1] - sorted_numbers[i]):
				min_unfairness = (sorted_numbers[i + k - 1] - sorted_numbers[i])
		
		return min_unfairness


if __name__ == '__main__':
	n = int(input())
	k = int(input())
	numbers = list()
	for i in range(n):
		numbers.append(int(input()))
	# end of for
	print(compute_unfairness(n, k, numbers))

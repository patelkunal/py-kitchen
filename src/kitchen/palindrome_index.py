def is_palindrome(inp):
    if len(inp) < 2:
        return True
    if inp[0] != inp[-1]:
        return False
    return is_palindrome(inp[1:-1])


def find_palindrome_index_naive(inp):
    if is_palindrome(inp):
        return -1
    for i, ch in enumerate(inp):
        new_word = inp[0:i] + inp[i+1:]
        if is_palindrome(new_word):
            return i
    return -1

def find_palindrome_index(s):
    if len(s) < 2:
        return -1
    l = len(s)
    for i in range(l//2):
        if s[i] != s[l - (i + 1)]:
            if s[i+1] == s[l - (i + 1)]:
                return i
            elif s[i] == s[l - (i + 2)]:
                return (l - (i + 1))
    return -1

def find_palindrome_index_best(s):
    if len(s) < 2:
        return -1
    i, j = 0, len(s) - 1
    while i < j and s[i] == s[j]:
        i += 1
        j -= 1
    if s[i] == s[j]:
        return -1
    elif s[i + 1] == s[j]:
        return i
    else:
        return j


if __name__ == "__main__":
    tests = int(input())
    for t in range(tests):
        #print(find_palindrome_index_naive(input()))
        print(find_palindrome_index(input()))
    # end-of-for
